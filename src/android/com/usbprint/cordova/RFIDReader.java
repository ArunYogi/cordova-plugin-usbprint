package com.usbprint.cordova;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbDeviceConnection;
import android.util.Log;
import android.widget.Toast;
import android.support.v4.view.MotionEventCompat;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

public class RFIDReader {

    private static final String TAG = "USBPrint";
    protected static final String APP = "com.gokhana.connection.USB_PERMISSION";
    protected static final String ACTION_USB_DETACHED = "android.hardware.usb.action.USB_DEVICE_DETACHED";
    protected static final String ACTION_USB_ATTACHED = "android.hardware.usb.action.USB_DEVICE_ATTACHED";
    public static final int MAX_GLOBAL_BUFF_LEN = 30720;
    public static final int USB_TRANS_MAX_DATA_LEN = 62;
    public static final int USB_TRANS_MAX_LEN = 64;
    public static final int USB_TRANS_TIMEOUT = 2000;
    public static int[] PACKET = new int[9216];
    
    public static final int PARAMETER_ERR = -1001;
    public static final int READ_ERR = -1002;
    public static final int CRC_ERR = -1007;
    public static final int FORMART_ERR = -1009;
    public static final int GET_HIDSTRUCT_ERR = -1004;
    public static final int HIDINIT_ERR = -1005;
    public static final int HID_TIMEOUT = -1006;
    public static final int MODE_ERR = -1008;

    private boolean connected = false;
    private UsbDevice device;
    private UsbManager usbManager;
    private UsbEndpoint epIn;
    private UsbEndpoint epOut;
    private UsbInterface usbInterface = null;
    private UsbDeviceConnection conn = null;
    private CallbackContext callbackContext;
    public static boolean CARD_MODE = true;

    private int PRODUCT_ID = 33335;
    private int VENDOR_ID = 4292;
    private int INTERFACE_CLASS = 3;
    private int INTERFACE_PROTOCOL = 2;
    private int INTERFACE_SUB_CLASS = 1;
    private boolean isOpen = false;
    private boolean registeredForBroadcast = false;
    private int availableLen;

    private byte[] globalBuf = new byte[MAX_GLOBAL_BUFF_LEN];
    private byte[] rcvBuffer = new byte[64];
    private byte[] sndBuffer = new byte[64];

    private PendingIntent pendingIntent;
    public Context applicationContext;

    private final BroadcastReceiver rfidEventReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Toast.makeText(RFIDReader.this.applicationContext, "action: " + action, Toast.LENGTH_SHORT).show();
            if (!RFIDReader.this.ACTION_USB_ATTACHED.equals(action)) {
                if (RFIDReader.this.APP.equals(action)) {
                    synchronized (this) {
                        UsbDevice usbDevice = (UsbDevice) intent.getParcelableExtra("device");
                        if (intent.getBooleanExtra("permission", false)) {
                            RFIDReader.this.openDevice();
                        } else {
                            Toast.makeText(RFIDReader.this.applicationContext, "Deny USB Permission", 0).show();
                            Log.d(RFIDReader.this.TAG, "permission denied");
                        }
                    }
                }
                if (RFIDReader.this.ACTION_USB_DETACHED.equals(action)) {
                    Toast.makeText(RFIDReader.this.applicationContext, "RFID card Disconnect", 0).show();
                    RFIDReader.this.closeDevice();
                    return;
                }
                Log.d(RFIDReader.this.TAG, "Processed USB Event");
            }
        }
    };

    public RFIDReader(UsbManager usbManager, Context context, CallbackContext callbackContext) {
        this.usbManager = usbManager;
        this.callbackContext = callbackContext;
        this.applicationContext = context;
        this.init();
    }

    private boolean init() {
        this.isOpen = false;
        enumerateDevice();
        findInterface();
        openDevice();
        assignEndpoint();
        return this.isOpen;
    }

    public void setCallbackContext(CallbackContext callbackContext) {
        this.callbackContext = callbackContext;
    }

    public synchronized void closeDevice() {
        try {
            Thread.sleep(10);
        } catch (Exception e) {
        }
        if (this.conn != null) {
            if (this.usbInterface != null) {
                this.conn.releaseInterface(this.usbInterface);
                this.usbInterface = null;
            }
            this.conn.close();
        }
        if (this.device != null) {
            this.device = null;
        }
        if (this.registeredForBroadcast && this.rfidEventReceiver != null) {
            this.applicationContext.unregisterReceiver(this.rfidEventReceiver);
            this.registeredForBroadcast = false;
        }
        this.isOpen = false;
    }

    public boolean doesRFIDReaderAvailable() {
        if (this.device != null) { return true; }
        else { return false; }
    }

    private void enumerateDevice() {
        if (this.usbManager != null) {
            HashMap<String, UsbDevice> deviceList = this.usbManager.getDeviceList();
            if (!deviceList.isEmpty()) {
                for (UsbDevice device : deviceList.values()) {
                    if (device.getVendorId() == this.VENDOR_ID && device.getProductId() == this.PRODUCT_ID) {
                        this.device = device;
                        Log.d(TAG, "Identified RFID reader");
                        Toast.makeText(this.applicationContext, "RFID Device identified", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    private void findInterface() {
        if (this.device != null && 0 < this.device.getInterfaceCount()) {
            UsbInterface intf = this.device.getInterface(0);
            if (intf.getInterfaceClass() == this.INTERFACE_CLASS && intf.getInterfaceSubclass() == this.INTERFACE_SUB_CLASS && intf.getInterfaceProtocol() == this.INTERFACE_PROTOCOL) {
                this.usbInterface = intf;
                Log.d(TAG, "Found RFID reader interface");
                Toast.makeText(this.applicationContext, "Identified RFID Device Interface", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /* access modifiers changed from: private */
    public void openDevice() {
        this.pendingIntent = PendingIntent.getBroadcast(this.applicationContext, 0, new Intent(this.APP), 0);
        IntentFilter filter = new IntentFilter(this.APP);
        filter.addAction(this.ACTION_USB_DETACHED);
        this.applicationContext.registerReceiver(this.rfidEventReceiver, filter);
        this.registeredForBroadcast = true;
        if (this.usbInterface != null) {
            UsbDeviceConnection conn = null;
            if (this.usbManager.hasPermission(this.device)) {
                Toast.makeText(this.applicationContext, "Has Permission to open RFID Device Interface", Toast.LENGTH_LONG).show();
                conn = this.usbManager.openDevice(this.device);
            } else {
                Toast.makeText(this.applicationContext, "Dont have Permission to open RFID Device Interface", Toast.LENGTH_LONG).show();
                this.usbManager.requestPermission(this.device, this.pendingIntent);
            }
            if (conn != null) {
                Toast.makeText(this.applicationContext, "Claiming Interface of RFID device", Toast.LENGTH_SHORT).show();
                if (conn.claimInterface(this.usbInterface, true)) {
                    this.conn = conn;
                    this.isOpen = true;
                    Log.d(TAG, "RFID reader device opened");
                    Toast.makeText(this.applicationContext, "Claimed Interface of RFID device", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(this.applicationContext, "Coudnt claim Interface of RFID device so disconnecting", Toast.LENGTH_SHORT).show();
                conn.close();
            }
        }
    }

    private void assignEndpoint() {
        if (this.usbInterface != null) {
            for (int i = 0; i < this.usbInterface.getEndpointCount(); i++) {
                UsbEndpoint ep = this.usbInterface.getEndpoint(i);
                if (ep.getType() == 3) {
                    if (ep.getDirection() == 0) {
                        this.epOut = ep;
                        Toast.makeText(this.applicationContext, "Indentified Output endgpoint of RFID device", Toast.LENGTH_SHORT).show();
                    } else {
                        this.epIn = ep;
                        Toast.makeText(this.applicationContext, "Indentified Input endgpoint of RFID device", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
        Log.d(TAG, "assignEndpoint suc");
    }

    public boolean UsbFeatureSupported() {
        return this.applicationContext.getPackageManager().hasSystemFeature("android.hardware.usb.host");
    }

    public int scan(int[] cardData) {
        if (this.preset(0) >= 0) {
            int iRet = this._scan(cardData);
            return iRet;
        } else {
            return -1005;
        }
    }

    private int _scan(int[] cardNoData) {
        int[] tmpCardNo = new int[32];
        int[] data = {3};
        if (this.sendData(PACKET, buildPackge(data, data.length)) < 0) {
            Toast.makeText(RFIDReader.this.applicationContext, "scan of RFID reader is fine", 0).show();
            return -1005;
        }
        int iret = resolveConfigData(tmpCardNo);
        if (iret < 0) {
            return iret;
        }
        if (tmpCardNo[0] == 11) {
            CARD_MODE = false;
        } else {
            CARD_MODE = true;
        }
        int cardNoDataLen = iret;
        for (int i = 0; i < cardNoDataLen; i++) {
            cardNoData[i] = tmpCardNo[(cardNoDataLen - 1) - i];
        }
        return cardNoDataLen;
    }

    private static void clear() {
        Arrays.fill(PACKET, 0);
    }
    
    public static int buildPackge(int[] configData, int dataLen) {
        clear();
        int tmpLen1 = dataLen + 4;
        int i = 0 + 1;
        PACKET[0] = 8;
        int i2 = i + 1;
        PACKET[i] = 80;
        int i3 = i2 + 1;
        PACKET[i2] = (byte) (tmpLen1 / 256);
        int i4 = i3 + 1;
        PACKET[i3] = (byte) (tmpLen1 % 256);
        int i5 = i4 + 1;
        PACKET[i4] = 0;
        int i6 = i5 + 1;
        PACKET[i5] = 0;
        int i7 = i6 + 1;
        PACKET[i6] = (byte) ((dataLen + 1) % 256);
        int j = 0;
        while (j < dataLen) {
            PACKET[i7] = configData[j];
            j++;
            i7++;
        }
        int tmpCheck = 0;
        for (int j2 = 0; j2 < dataLen; j2++) {
            tmpCheck ^= configData[j2];
        }
        int i8 = i7 + 1;
        PACKET[i7] = tmpCheck ^ ((byte) ((dataLen + 1) % 256));
        return i8;
    }

    public static int resolveConfigData(int[] configData) {
        int configDataLen = PACKET[5] + 2;
        int[] tmpints = new int[configDataLen];
        System.arraycopy(PACKET, 4, tmpints, 0, configDataLen);
        if (tmpints[0] != 254) {
            return PARAMETER_ERR;
        }
        int tmpCheck = 0;
        for (int i = 0; i < configDataLen - 2; i++) {
            tmpCheck ^= tmpints[i + 1];
        }
        if (tmpCheck != tmpints[configDataLen - 1]) {
            return FORMART_ERR;
        }
        if ((tmpints[2] & 224) != 0) {
            return -1010 - tmpints[2];
        }
        System.arraycopy(tmpints, 3, configData, 0, configDataLen - 4);
        return configDataLen - 4;
    }

    private void initGlobalBuf(int[] intArr, int arrLen) {
        this.availableLen = 0;
        for (int n = 0; n < 30720; n++) {
            this.globalBuf[n] = 0;
        }
        for (int n2 = 0; n2 < arrLen; n2++) {
            this.globalBuf[n2] = (byte) intArr[n2];
        }
        this.availableLen = arrLen;
    }

    public void ucharp2jintp(int[] intArr, byte[] ucharp, int arrLen) {
        for (int n = 0; n < arrLen; n++) {
            intArr[n] = ucharp[n] & 255;
        }
    }

    public int sendData(int[] data, int len) {
        int max;
        if (!this.isOpen && !this.init()) {
            return -1;
        }
        initGlobalBuf(data, len);
        byte header = this.globalBuf[0];
        byte cmd = this.globalBuf[1];
        int leftLen = this.availableLen - 2;
        int startIndex = 2;
        while (leftLen >= 0) {
            for (int i = 0; i < 64; i++) {
                this.sndBuffer[i] = 0;
            }
            this.sndBuffer[0] = header;
            if (leftLen == this.availableLen - 2) {
                this.sndBuffer[1] = cmd;
            } else {
                this.sndBuffer[1] = (byte) (cmd | 128);
            }
            System.arraycopy(this.globalBuf, startIndex, this.sndBuffer, 2, 62);
            Log.d(TAG, "data send: " + converB2String(this.sndBuffer, 62));
            int iRet = this.conn.bulkTransfer(this.epOut, this.sndBuffer, this.sndBuffer.length, USB_TRANS_TIMEOUT);
            if (iRet < 0) {
                return iRet;
            }
            leftLen -= 62;
            startIndex += 62;
        }
        for (int i2 = 0; i2 < 64; i2++) {
            this.rcvBuffer[i2] = 0;
        }
        Arrays.fill(this.rcvBuffer, (byte) 0);
        Arrays.fill(this.globalBuf, (byte) 0);
        int iRet2 = this.conn.bulkTransfer(this.epIn, this.rcvBuffer, this.rcvBuffer.length, USB_TRANS_TIMEOUT);
        if (iRet2 < 0) {
            return iRet2;
        }
        Log.d(TAG, "data received:" + converB2String(this.rcvBuffer, 62));
        int leftLen2 = ((this.rcvBuffer[2] & 255) * 256) + (this.rcvBuffer[3] & 255);
        int dataValidLen = leftLen2 + 4;
        int leftLen3 = leftLen2 - 60;
        System.arraycopy(this.rcvBuffer, 0, this.globalBuf, 0, 64);
        int startIndex2 = 0 + 64;
        while (leftLen3 > 0) {
            Arrays.fill(this.rcvBuffer, (byte) 0);
            int iRet3 = this.conn.bulkTransfer(this.epIn, this.rcvBuffer, this.rcvBuffer.length, USB_TRANS_TIMEOUT);
            if (iRet3 >= 0) {
                Log.d(TAG, "data received:" + converB2String(this.rcvBuffer, 62));
                System.arraycopy(this.rcvBuffer, 2, this.globalBuf, startIndex2, 62);
                leftLen3 -= 62;
                startIndex2 += 62;
            } else {
                Log.d(TAG, "iRet:" + String.valueOf(iRet3));
                return iRet3;
            }
        }
        if (dataValidLen > len) {
            max = dataValidLen;
        } else {
            max = len;
        }
        for (int i3 = 0; i3 < max; i3++) {
            data[i3] = 0;
        }
        ucharp2jintp(data, this.globalBuf, dataValidLen);
        return dataValidLen;
    }

    public int reset() {
        int[] tmpCardNo = new int[32];
        int[] data = {1};
        if (this.sendData(PACKET, buildPackge(data, data.length)) < 0) {
            return -1005;
        }
        return resolveConfigData(tmpCardNo);
    }

    public int preset(int inMode) {
        int[] data = new int[2];
        int[] tmpBuf = new int[128];
        data[0] = 2;
        if (inMode == 0) {
            data[1] = 3;
        } else {
            data[1] = 0;
        }
        if (this.sendData(PACKET, buildPackge(data, data.length)) < 0) {
            return -1005;
        }
        int iret = resolveConfigData(tmpBuf);
        if (iret < 0) {
            return iret;
        }
        if (inMode != 0) {
            return iret;
        }
        int[] data1 = {12};
        Arrays.fill(tmpBuf, 0);
        if (this.sendData(PACKET, buildPackge(data1, data1.length)) >= 0) {
            return resolveConfigData(tmpBuf);
        }
        return -1005;
    }

    public static String converB2String(byte[] data, int length) {
        if (length < 1) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int n = 0; n < length; n++) {
            String hex = Integer.toHexString(data[n] & 255);
            if (hex.length() == 1) {
                hex = String.valueOf('0') + hex;
            }
            sb.append(new StringBuilder(String.valueOf(hex.toUpperCase())).toString());
            if (n > 0 && (n + 1) % 16 == 0) {
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public static String convert2String(int[] data, int length) {
        if (length < 1) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int n = 0; n < length; n++) {
            String hex = Integer.toHexString(data[n] & MotionEventCompat.ACTION_MASK);
            if (hex.length() == 1) {
                hex = String.valueOf('0') + hex;
            }
            sb.append(new StringBuilder(String.valueOf(hex.toUpperCase())).toString());
            if (n > 0 && (n + 1) % 16 == 0) {
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public static int[] convertStringToIntArr(String hexString) {
        int strLen = hexString.length();
        int len = strLen % 2 == 0 ? strLen / 2 : (strLen / 2) + 1;
        int[] data = new int[len];
        String tmpStr = hexString;
        if (strLen % 2 == 1) {
            tmpStr = String.valueOf(tmpStr) + "0";
        }
        int i = 0;
        while (i < len) {
            try {
                data[i] = Integer.parseInt(tmpStr.substring(i * 2, (i * 2) + 2), 16);
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return data;
    }

    public static int img_One2Two(byte[] psFrom, byte[] psTo, int psFromLen) {
        int i = 0;
        while (i < psFromLen) {
            psTo[i * 2] = (byte) ((psFrom[i] & 15) << 4);
            psTo[(i * 2) + 1] = (byte) (psFrom[i] & 240);
            i++;
        }
        return i * 2;
    }
}